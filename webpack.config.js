const miniCss = require('mini-css-extract-plugin');

module.exports = {
  mode: 'none',
  entry: {
    background: './src/background.js',
    options: './src/options_page/options.js',
    edit_url_popup: './src/edit_url_popup_page/edit_url_popup.js',
    content_scripts: './src/content_script.js'
  },
  output: {
    path:     `${__dirname}/`,
    filename: 'extension/[name]/js/bundle.js',
  },
  module: {
    rules: [
      {
        test:/\.(s*)css$/,
        use: [
          miniCss.loader,
          'css-loader',
          'sass-loader',
       ]
     }
    ],
  },
  plugins: [
    new miniCss({
       filename: './extension/[name]/styles/style.css',
    }),
 ],
  resolve: {
    extensions: ['.js'],
  },
  devtool: 'source-map',
};