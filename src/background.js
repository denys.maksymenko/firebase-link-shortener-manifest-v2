const Browser = require("webextension-polyfill");
const _ = require('lodash');
const service = require('./service');

const handleClickIcon = async (tab) => {
    const currentTab = await service.getCurrentTab();
    const url = _.get(currentTab, 'url', null);
    const title = _.get(currentTab, 'title', null);
    const tabId = _.get(currentTab, 'id', null);
    await service.makeShortAndCopy({title, url, tabId, refreshShortLink: false})
};

service.getOptions().then(async (options) => {
    if (options.isEditUrl) {
        await Browser.browserAction.setPopup({popup: '../../edit_url_popup/edit_url_popup.html'});
    } else {
        await Browser.browserAction.setPopup({popup: ''});
        Browser.browserAction.onClicked.addListener(handleClickIcon);
    }
});

